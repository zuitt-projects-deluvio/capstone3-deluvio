import {useState, useEffect} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import CourseView from './components/CourseView';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Courses from './pages/Courses';
import LogIn from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Cart from './pages/Cart';
import {UserProvider} from './UserContext';
import OrderHistory from './pages/OrderHistory'




function App() {

    const [user, setUser] = useState({
        id: null,
        isAdmin: null
      })
   
   const [movieName, setMovieName] = useState('');

   const [currentCart, setCurrentCart] = useState([]);

    const [cartCount, modifCount] = useState(0);
    const [isAdded, setIsAdded] = useState(false);


    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    const [movImg, setMovImg] = useState('')


    const unsetUser = () => {
        localStorage.clear()
    }

    useEffect(() => {
        fetch('https://fathomless-forest-51958.herokuapp.com/users/details', {
            method: "GET",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);

            if(typeof data._id !== "undefined"){
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            } else {
                setUser({
                    id: null,
                    isAdmin: null
                })
            }

            if(localStorage.getItem('cartCount') !== null){
                let counter = Number(localStorage.getItem('cartCount'))
                modifCount(counter)
            } else {
                modifCount(null)
            }
        })
    }, [])

    // console.log(user)

    return ( 

    <UserProvider value={{currentCart, setCurrentCart, movieName, setMovieName, user, setUser, unsetUser, isAdded, setIsAdded, movImg, setMovImg, cartCount, modifCount, name, setName, description, setDescription, price, setPrice}}>

        <Router>

        
                <AppNavbar />

                    <Routes>

                        <Route exact path="/" element={<Home />}/>
                        <Route exact path="/courses" element= {<Courses />}/>
                        <Route exact path="/courses/:movieId" element= {<CourseView />}/>

                        <Route exact path="/cart" element ={<Cart />}/>

                        <Route exact path="/orderHistory" element ={<OrderHistory />}/>

                       {/*  <Route exact path="/dashboard" element ={<Dashboard />}/>

                         <Route exact path="/mycourses" element ={<MyCourses />}/>*/}

                        <Route exact path="/register" element= {<Register />}/>
                        <Route exact path="/login" element= {<LogIn />}/>
                        <Route exact path="/logout" element= {<Logout />}/>
                        <Route path="*" element={<Error />} />

                    </Routes>

         

        </Router>  

    </UserProvider>
    )
}

export default App;
