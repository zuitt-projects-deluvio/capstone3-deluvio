


export default function OrderCard({name, price, movieImg}) {

return (
		<div className="row my-3">
			
			<div className="col flex flex-row items-center">
				<img className="img-fluid w-25 height:auto" src={movieImg} />
				<h5 className="m-4">{name}</h5>
			</div>
			<div className="col-4 flex flex-row justify-center items-center">
				<h6>{price}</h6>
			</div>
		</div>
	)

}