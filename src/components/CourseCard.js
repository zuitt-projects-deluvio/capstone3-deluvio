import {Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard ({movieData}) {

	const {movieName, movieDescription, price, _id, movieImg} = movieData;


	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={12}>
				<Card className="cardHighlight p-3">
					
					<Card.Body className="row">
						<div className="my-2 text-center col-3">
							<img className="img-fluid w-50 height:auto" src={movieImg} />
							
						</div>
                  <div className="col">
                     <Card.Title>{movieName}</Card.Title>
                     <Card.Subtitle>Description:</Card.Subtitle>
                     <Card.Text>{movieDescription}</Card.Text>

                     <Card.Subtitle>Price:</Card.Subtitle>
                     <Card.Text>{`PhP ${price}`}</Card.Text>
                     
                     <Button variant="primary" as={Link} to={`/courses/${_id}`}>
                        See details
                     </Button>
                  </div>

					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}

