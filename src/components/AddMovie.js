import {Container, Form, Button} from 'react-bootstrap';

import CourseCard from '../components/CourseCard';
import {useEffect, useState, useContext} from 'react';
import UserContext from './../UserContext';

import Swal from 'sweetalert2';


export default function AddMovie () {

	// for adding movies

   const {movieName, setMovieName} = useContext(UserContext);
	
	const [movieDescription, setMovieDescription] = useState('');
	const [price, setPrice] = useState('');
	const [movieImg, setMovieImg] = useState('');

	const [isActive, setIsActive] = useState(false);
	// for adding movies

	// checking if all fields are filled

	useEffect(() => {

		if(movieName !== '' && movieDescription !== '' && price !== '' && movieImg !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [movieName, movieDescription, price, movieImg])

	// checking if all fields are filled

	function addMovie (e) {
		e.preventDefault();

		fetch('https://fathomless-forest-51958.herokuapp.com/movies/addMovie', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				movieName: movieName,
				movieDescription: movieDescription,
				price: price,
				movieImg: movieImg,
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(typeof data,'this data')

			if(typeof data === "object") {
				setMovieName('');
				setMovieDescription('');
				setPrice('');
				setMovieImg('');

				Swal.fire({
					title: 'Add Movie',
					icon: 'success',
					text: 'Successfully Added Movie'
				})

				
			} else {

				setMovieName('');
				setMovieDescription('');
				setPrice('');
				setMovieImg('');

				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})



	}

	return (

			<div>
				<h1 className="my-4">Add Movies</h1>
				<Form className="my-4" onSubmit={e => addMovie(e)}>

					<Form.Group controlId="movieName">
						<Form.Label>Movie</Form.Label>
						<Form.Control 
							type = "text"
							placeholder = "Enter Movie Name"
							value={movieName}
							onChange={(e) => setMovieName(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="movieDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control 
							type = "text"
							placeholder = "Enter movie description"
							value={movieDescription}
							onChange={(e) => setMovieDescription(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="price">
						<Form.Label>Price</Form.Label>
						<Form.Control 
							type = "number"
							placeholder = "Input movie price"
							value={price}
							onChange={(e) =>setPrice(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="movieImg">
						<Form.Label>Movie Image (URL Adress)</Form.Label>
						<Form.Control 
							type = "text"
							placeholder = "Input movie image URL address"
							value={movieImg}
							onChange={(e) =>setMovieImg(e.target.value)}
							required
						/>
					</Form.Group>

					{isActive ?
						<Button 
							variant="success" 
							type="submit" 
							id="submitBtn"
							className="mt-3"
						>
							Add Movie
						</Button>
						:
						<Button 
							variant="success" 
							type="submit" 
							id="submitBtn"
							className="mt-3 disabled"
						>
							Add Movie
						</Button>
					}
					 
				</Form>
			</div>

		)
}