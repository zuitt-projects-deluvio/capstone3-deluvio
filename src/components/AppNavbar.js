import React, {useState, useContext, useEffect} from 'react';
import {Navbar, Container, Nav, Badge} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const {user, cartCount, modifCount} = useContext(UserContext);
	// console.log(user.isAdmin)

   // const [] = useState({})

   // useEffect(  ,[user])

	return(
		<nav className="navbar bg-black">
			<div className="">
				<Link className="text-deco-none  text-light" to="/">
					<h1 >CouchPotato</h1>
				</Link>
			</div>

			<div className="links text-deco-none ">

				<div >
					<Link className="nav-links-item" to="/">Home</Link>
					<Link className="nav-links-item" to="/courses">Movies</Link>
					
					{/*{ (user.isAdmin !== undefined && user.isAdmin === true) &&
					<Link className="nav-links-item" to="/dashboard">Dashboard</Link>
					}*/}

					
				</div>

				{ (user.isAdmin !== undefined && user.isAdmin === true) ?
					<></>
					:
					 user.id !== null &&
					 <>
					 	<Link className="nav-links-item" to="/orderHistory">OrderHistory</Link>
					</>
				}

				{ (user.id !== null) ?
						<Link className="nav-links-item" to="/logout">Logout</Link>
						:
						<>
							<Link className="nav-links-item" to="/register">Register</Link>
							<Link className="nav-links-item" to="/login">Login</Link>
						</>
				}

				{ (user.isAdmin !== undefined && user.isAdmin === true) ?
					<></>
					:
					 user.id !== null &&
					 <>
						<Link to="/cart" className="cart-icon"> 		
							<svg  xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white"   viewBox="0 0 16 16">
							  <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
							</svg>
							<div className="badge">
								{ cartCount !== null &&
								<Badge pill bg="danger">{cartCount}</Badge>
								}
							</div>
						</Link>

					</>
					
				}

			</div>
		</nav>


	)
}

// {/*<Navbar bg="light" expand="lg">
// 		  <Container>
// 		    {/*<Navbar.Brand as={Link} to="/" href="/">Zuitt</Navbar.Brand>*/}
// 		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
// 		    <Navbar.Collapse id="basic-navbar-nav">
// 		      <Nav className="ml-auto">
// 		        <Nav.Link as={Link} to="/">Home</Nav.Link>
// 		        <Nav.Link as={Link} to="/courses">Courses</Nav.Link>

// 				{ (user.id !== null) ?
// 					<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
// 					:
// 					<>	
// 				        <Nav.Link as={Link} to="/register">Register</Nav.Link>
// 				        <Nav.Link as={Link} to="/login">LogIn</Nav.Link>
// 				    </>
// 				}

// 		      </Nav>
// 		    </Navbar.Collapse>
		   
// 		  </Container>
		  
// 		</Navbar>*/}