import {useState, useEffect, useContext} from 'react';
import {useParams, useNavigate, Link} from 'react-router-dom';
import {Container, Card, Col, Row, Button, Form} from 'react-bootstrap'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function CourseView () {

	const {user, isAdded, currentCart, setCurrentCart, setIsAdded, cartCount, modifCount, name, setName, movImg, setMovImg, description, setDescription, price, setPrice} = useContext(UserContext);


	//allows us to gain access to methods that will allow us to redirect the user to a different page after enrolling a course
	const history = useNavigate();

	const {movieId} = useParams();

	


	const addToCart = (movieId) => {

      let cartEntry = [
         {
            movieId: movieId,
            movieName: name,
            movieDescription: description,
            price: price,
            movieImg: movImg
         }
      ]

      let cartEntry2 = {
            movieId: movieId,
            movieName: name,
            movieDescription: description,
            price: price,
            movieImg: movImg
         }
      
      
		localStorage.setItem(movieId, name)
		setIsAdded(true);
		// console.log(cartCount)
		if(localStorage.getItem('cartCount') !== null){

			localStorage.setItem('cartCount', cartCount + 1);
			modifCount(cartCount + 1);

		} else {
			localStorage.setItem('cartCount', 1)
			modifCount(1)
		}

		if(localStorage.getItem('moviesAdded') !== null){

			let existingData = JSON.parse(localStorage.getItem('moviesAdded'));
			existingData.push(cartEntry2);
			localStorage.setItem('moviesAdded', JSON.stringify(existingData));
			
		} else {
			localStorage.setItem('moviesAdded', JSON.stringify(cartEntry))
		}
		
	}

	useEffect(() => {	
		// console.log(courseId)
		fetch(`https://fathomless-forest-51958.herokuapp.com/movies/getSingleMovie/${movieId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setName(data.movieName);
			setDescription(data.movieDescription);
			setPrice(data.price);
			setMovImg(data.movieImg);

			if(localStorage.getItem(movieId) !== null){
				setIsAdded(true);
			} else {
				setIsAdded(false)
			}

		})
	}, [movieId])

	// console.log(user.isAdmin)

	const [desc, setDesc] = useState(description)
	const [title, setTitle] = useState(name)
	const [thePrice, setThePrice] = useState(price)
	const [movUrl, setMovUrl] = useState(movImg)
	const [editInfo, setEditInfo] = useState(false)


	function cancelEdit() {
		setEditInfo(false)
	}

	function editDetails() {
		setEditInfo(true)
	}	


	function submitChanges() {

		fetch(`https://fathomless-forest-51958.herokuapp.com/movies/updateMovieInfo/${movieId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				movieName: title,
				movieDescription: desc,
				price: thePrice,
				movieImg: movUrl

			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			
			Swal.fire({
				title: 'Edit Movie Information',
				icon: 'success',
				text: 'Successfully edited movie information'
			}).then(() => {

				cancelEdit();
				window.location.reload(false);
			})
		})

	}


	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card>
						<Card.Body>
							<div className="my-2 text-center">
								<img className="img-fluid w-25 height:auto" src={movImg} />
							</div>

							<Card.Title className="my-2">{name}</Card.Title>
                     {editInfo &&
								<>
                           <Card.Subtitle className="my-2">Title</Card.Subtitle>
                           <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                              <Form.Control as="input"  onChange={(e) => setTitle(e.target.value)} rows={3} />
                           </Form.Group>
								</>
							}

							<Card.Subtitle>Description</Card.Subtitle>
                     <Card.Text>{description}</Card.Text>
							{editInfo &&
								<Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
								    <Form.Control as="textarea"  onChange={(e) => setDesc(e.target.value)} rows={3} />
								 </Form.Group>
							}
							
							<Card.Subtitle>Price</Card.Subtitle>
                     <Card.Text>{price}</Card.Text>
							{editInfo &&
								<Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
								    <Form.Control as="input" type="number"  onChange={(e) => setThePrice(e.target.value)} rows={3} />
								 </Form.Group>
							}

                     {user.isAdmin && 
                        <>
                        <Card.Subtitle>Movie Image Url</Card.Subtitle>
                        <Card.Text>{movImg}</Card.Text>
                        {editInfo &&
                           <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                              <Form.Control as="input" onChange={(e) => setMovUrl(e.target.value)} rows={3} />
                           </Form.Group>
                        }
                        </>
                     }

							{user.id !== null ? 	

									user.isAdmin ?

											editInfo ? 
												<>
													<Button variant="danger" onClick={() => {cancelEdit()}}>Cancel Edit</Button>
													<Button className="mx-2" variant="success" onClick={() => {submitChanges()}}>Submit changes</Button>
												</>
											:
												<Button variant="warning" onClick={() => {editDetails()}}>Edit Details</Button>

										:
											isAdded ?
												<Button variant="success" disabled >Added to cart</Button>
											:
												<Button variant="primary" onClick={() => {
													addToCart(movieId)
												}} >Add to cart</Button>

								:
								<Link className="btn btn-danger" to="/login">Log In To Buy Movie</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}

{/*<Button variant="primary" onClick={()=> 
buyMovie(movieId)}>Buy Movie</Button>*/}

 // 