import {Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';
import UserContext from './../UserContext';

// import Courses from '../data/coursesData';

export default function CourseCardAdmin ({movieData, archive, activate }) {

	const {movieName, movieDescription, price, _id, isActive, movieImg} = movieData;
	// console.log(movieData)
	const {user} = useContext(UserContext);

	const [isArchiveActive, setIsArchiveActive] = useState(isActive)


	const archiveMovie = () => {
		fetch(`https://fathomless-forest-51958.herokuapp.com/movies/archiveMovie/${_id}`,{
	        method: "PUT",
	        headers: {
	            Authorization: `Bearer ${localStorage.getItem("token")}`
	        }
	    }).then(data => {
	    	// console.log(data.isActive)
	    	setIsArchiveActive(false)
	    })
	}

	const activateMovie = () => {
		fetch(`https://fathomless-forest-51958.herokuapp.com/movies/activateMovie/${_id}`,{
	        method: "PUT",
	        headers: {
	            Authorization: `Bearer ${localStorage.getItem("token")}`
	        }
	    }).then(data => {
	    	// console.log(data.isActive)
	    	setIsArchiveActive(true)
	    })
	}

	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={12}>
				<Card className="cardHighlight p-3">
					
					<Card.Body className="row">
						<div className="my-2 text-center col-3">
							<img className="img-fluid w-50 height:auto" src={movieImg} />
						</div>
                  <div className="col">
                     <Card.Title>{movieName}</Card.Title>
                     <Card.Subtitle>Description:</Card.Subtitle>
                     <Card.Text>{movieDescription}</Card.Text>

                     <Card.Subtitle>Price:</Card.Subtitle>
                     <Card.Text>{`PhP ${price}`}</Card.Text>
                     <Button className="mx-2" variant="primary" as={Link} to={`/courses/${_id}`}>
							See details
						   </Button>
                     {isArchiveActive ?
							<Button variant="danger" onClick={() => archiveMovie()}>
								Archive Movie
							</Button>
							:
							<Button variant="primary" onClick={() => activateMovie()}>
								Activate Movie
							</Button>
						   }
                  </div>
						
						

						
						
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}

