import {Button} from 'react-bootstrap';
import {useContext} from 'react';
import UserContext from '../UserContext';
import {useNavigate} from 'react-router-dom';

export default function CartItem({name, price, movieImg, id}) {

   const {cartCount, modifCount} = useContext(UserContext)

   const history = useNavigate();
  

   function removeCartItem () {
   
      localStorage.removeItem(id);
      if(localStorage.getItem('cartCount') === "1"){
         modifCount(0)
         localStorage.removeItem('cartCount')
         history("/courses")
      } else {
         localStorage.setItem('cartCount', cartCount - 1);
			modifCount(cartCount - 1);
      }

      let theData = JSON.parse(localStorage.getItem('moviesAdded'));

      if(theData.length === 1){
         localStorage.removeItem('moviesAdded')
      } else {
         let existingData = JSON.parse(localStorage.getItem('moviesAdded'));
         
         let theFilter = existingData.findIndex(item => {
               return item.movieId === id
            })
         existingData.splice(theFilter, 1)

         localStorage.setItem('moviesAdded', JSON.stringify(existingData));
         }
      
   }


	return (
			<div className="row my-3">
				<div className="col flex flex-row items-center">
					<img className="img-fluid w-25 height:auto" src={movieImg} />
					<h5 className="m-4">{name}</h5>
				</div>
				<div className="col-4 flex flex-row justify-center items-center">
					<h6>{price}</h6>
				</div>
            <div className="col-2 flex flex-row justify-center items-center">
               <Button variant="danger" onClick={() => {removeCartItem()}}>X</Button>
				</div>
				
			</div>
		)
}

