import {Container, Button} from 'react-bootstrap';
import {useEffect, useState} from 'react'
import OrderCard from '../components/OrderCard'


export default function OrderHistory () {


	const [allOrders, setAllOrders] = useState([]);


	useEffect(() => {
	    fetch('https://fathomless-forest-51958.herokuapp.com/orders/history', {
	        method: "GET",
	        headers: {
	            Authorization: `Bearer ${localStorage.getItem("token")}`
	        }
	    })
	    .then(res => res.json())
	    .then(data => {

	        setAllOrders(data.map(order => {
               const {_id, movieImg, movieName, price } = order;
               return <OrderCard key={_id} name={movieName} price={price} movieImg={movieImg}/>
	        }));

	    })
	}, [])

	return(
		
		<Container>
			<div className="row my-5">
				<div className="col pt-5 px-5 pb-5">
					<div className="row">
						<h4>Order History</h4>
						<hr/>
					</div>
					<div className="row">
						<div className="col">
							<h6>Product</h6>
						</div>
						<div className="col-4 text-center items-center">
							<h6>Price</h6>
						</div>
					</div>

					{allOrders}
					
				</div>
			</div>

		</Container>
	)
}