import {Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Home() {

		return(
			<>
				<section className="bg-section vh-100 flex justify-center items-center">
					<div className="center-texts text-light ">
						<h1>Unlimited movies, TV shows, and more.</h1>
						<h3>Watch anywhere. Cancel anytime.</h3>
						<h6>Ready to watch? Register now to start your membership.</h6>
						<Button className="m-3" as={Link} to="/register" variant="danger">Get Started</Button>
					</div>
				</section>
			</>
		)
	}
