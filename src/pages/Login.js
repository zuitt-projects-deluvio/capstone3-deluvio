import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext'
import {Container} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import AppNavbar from '../components/AppNavbar';

export default function LogIn() {

	const {user, setUser} = useContext(UserContext);
	// console.log(user);
   const history1 = useNavigate();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

   const {whatUser, setWhatUser} = useState();

	function authenticate(e){
		e.preventDefault();

		fetch('https://fathomless-forest-51958.herokuapp.com/users/login', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			
			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken);

            const retrieveUserDetails = (token) => {
               fetch('https://fathomless-forest-51958.herokuapp.com/users/details', 
               {
                  method: "GET",
                  headers: {
                     Authorization : `Bearer ${token}`
                  }
               })
               .then(res => res.json())
               .then(data => {
                  // console.log(data);
                  setUser({
                        id: data._id,
                        isAdmin: data.isAdmin
                  })
      
               })
            }
            
				retrieveUserDetails(data.accessToken)

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Couch Potato!'
				}).then(() => {
               history1("/courses")
            })
			} else {
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Please check your credentials'
				})
			}

        

		})

		// localStorage.setItem("email", email);

		

		// to access the user information it can be done using localStorage, this is necessary to update the user state which will help update the app component and rerender to avoid manually refreshing the page upon use login and logout

		//when state change componenets are rerendered and the AppNavBar component will be updated based on the user credentials.
		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		setEmail('');
		setPassword('');

		
	};

	useEffect(() => {

		if(email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email, password]);

	return (

		(user.id !== null) ?
		<Navigate to="/courses"/>
		:
		<Container>
		<Form className="my-4" onSubmit={e => authenticate(e)}>
			
			<h1>LogIn</h1> 
			<Form.Group className="mt-3" controlId="userEmail">
				<Form.Label>Email Address</Form.Label>

				<Form.Control
					type = "email"
					placeholder = "Enter your email here"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>

			</Form.Group>

			<Form.Group className="mt-3" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type = "password"
					placeholder = "Input your password here"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive ?
				<Button 
				variant="success" 
				type="submit" 
				id="submitBtn"
				className="mt-3"
				>
				Submit
				</Button>
				:
				<Button 
				variant="success" 
				type="submit" 
				id="submitBtn"
				className="mt-3 disabled"
				>
				Submit
				</Button>
			 }
		</Form>
		</Container>

	)
}