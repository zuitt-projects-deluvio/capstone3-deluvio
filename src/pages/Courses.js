// import coursesData from '../data/coursesData';
import {Container, Form, Button} from 'react-bootstrap';

import CourseCard from '../components/CourseCard';
import {useEffect, useState, useContext} from 'react';
import UserContext from './../UserContext';
import CourseCardAdmin from '../components/CourseCardAdmin';
import {Navigate, useNavigate} from 'react-router-dom';

import Swal from 'sweetalert2';
import AddMovie from '../components/AddMovie'


export default function Courses() {

	const [courses, setCourses] = useState([]);

	const [theMovies, setMovies] = useState(false);

	const {user, movieName, setMovieName} = useContext(UserContext);

	const [adminView, setAdminView] = useState([]);

	useEffect(() => {
		fetch('https://fathomless-forest-51958.herokuapp.com/movies/getActiveMovies')
		.then(res => res.json())
		.then(data => {
			
			
			if(data.message === "There are no active movies"){
				setMovies(false)

				
			} else if (typeof data === "object") {
				setMovies(true);
				setCourses(data.map(movie => {

					return (
						<CourseCard key={movie._id} movieData={movie}/>
					);
				}));
			}
			
		})
	}, [])

	
	useEffect(() => {
		fetch('https://fathomless-forest-51958.herokuapp.com/movies',{
	        method: "GET",
	        headers: {
	            Authorization: `Bearer ${localStorage.getItem("token")}`
	        }
	    })
		.then(res => res.json())
		.then(data => {
			// console.log(data, 'this is for admin only')
			// console.log(data.length !== 0, 'asdasdasd')

			if(data.auth === "Failed"){
				return false
			} else if(data.length === 0){
				return setMovies(false)
			} else if (data.length !== 0) {
				setMovies(true)
				setAdminView(data.map(movie => {

					return (
						<CourseCardAdmin key={movie._id} movieData={movie}/>
					);
				}));
			}

		})
	}, [movieName])


	const checker = () => {
		
		if(user.isAdmin === true){
				
			if(theMovies === true){
				return adminView
			} else {
				return <h1 className="text-center">There are no available movies</h1>
			}	

		} else {
			if(theMovies === true){
				return courses 
			} else {
				return <h1 className="text-center">There are no available movies</h1>
			}
		}
	}

	

	return (
		<Container>

			{user.isAdmin &&
				<AddMovie />
			}

			<h1 className="my-4">Movies</h1>

				{checker()}
				

		</Container>
	)

}