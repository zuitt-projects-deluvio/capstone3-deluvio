import {Container} from 'react-bootstrap';
import {Button} from 'react-bootstrap';
import {useContext, useState, useEffect} from 'react';
import {useNavigate, useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import CartItem from '../components/CartItem';
import Swal from 'sweetalert2'

export default function Cart(){
	
	const {cartCount, modifCount, currentCart, setCurrentCart} = useContext(UserContext);

	

	const [prices, setPrices] = useState([]);

	const history1 = useNavigate();

	useEffect(() => {
      console.log("use effect ran")
		const parsedMovies = JSON.parse(localStorage.getItem('moviesAdded'));
      console.log(parsedMovies)
      let thePrice = []

		if(parsedMovies !== null){
			setCurrentCart(parsedMovies.map(movie => {

               thePrice.push(movie.price);
               setPrices(thePrice)

               return (
                  <CartItem key={movie.movieId} name={movie.movieName} price={movie.price} movieImg={movie.movieImg} id={movie.movieId}/>
               )
            })
         )
		} else if (parsedMovies === null) {
         return setCurrentCart(<></>)
      }

	}, [cartCount]);


	function add(accumulator, a) {
		return accumulator + a;
	}
	
	const priceSum = prices.reduce(add, 0); // with initial value to avoid when the array is empty

	// const {courseId} = useParams();

	const buyMovie = async () => {	

		// let movieSuccess = [];
		// let movieFailed = [];

		const parsedMovies = JSON.parse(localStorage.getItem('moviesAdded'));


      const result = await fetch('https://fathomless-forest-51958.herokuapp.com/orders', {
         method: "POST",
         headers: {
            "Content-Type" : "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`
         },
         body: JSON.stringify({
            orderItems: parsedMovies,
            totalAmount: priceSum
         })
      })
      .then(res => res.json())
      .then(data => {

         Swal.fire({
            title: "Successfully Purchased",
            icon: "success",
            text: `You have successfully purchased the movies!`
         }).then(function() {
          
               history1("/courses")
               data.order.orderItems.map(id => {
                  localStorage.removeItem(id.movieId)
               })
               localStorage.removeItem('cartCount')
               localStorage.removeItem('moviesAdded')

               modifCount(0)
               // console.log(data)

           });
      })

		

}
	return(
		<Container>
			<div className="row my-5 shadow">
				<div className="col pt-5 px-5 pb-5">
					<div className="row">
						<h4>Items on Cart</h4>
						<hr/>
					</div>
					<div className="row">
						<div className="col">
							<h6>Product</h6>
						</div>
						<div className="col-4 text-center items-center">
							<h6>Price</h6>
						</div>
                  <div className="col-2 text-center items-center">
							<h6>Remove</h6>
						</div>
						
					</div>
				
					{currentCart}
					
				</div>

				<div className="col-4 pt-5 px-5 pb-5 checkout-bg">
					<div className="row">
						<h4>Order Summary</h4>
						<hr/>
					</div>
					<div className="row">
						<div className="col">
							<h6>Quantity</h6>
						</div>
						<div className="col text-center">
							<h6>Total</h6>
						</div>
					</div>
					<div className="row my-4">
						<div className="col">
							<h6>{(cartCount !== 0) && cartCount} Items</h6>
						</div>
						<div className="col text-center">
							<h6>{priceSum}</h6>
						</div>
					</div>
					<div className="flex items-end">
						{cartCount === 0 || cartCount === null ?
							<Button variant="success" className="mt-auto w-100" disabled>Checkout</Button>
							:
							<Button variant="success" className="mt-auto w-100" onClick={() => buyMovie()}>Checkout</Button>
						}
					</div>
				</div>
			</div>

		</Container>
	)
}